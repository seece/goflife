#ifndef BOARD_H
#define BOARD_H

#include <stdint.h>
#include <stdbool.h>

struct Board {
	int width, height;
	uint8_t* cells; // a bit vector for field data
	int size;
};

struct Board* board_make(int width, int height);
void board_free(struct Board* board);
void board_clear(struct Board* board);
static bool board_get_offset(const struct Board* board, int x, int y, int* out_byte_ofs, int* out_bit_ofs);
bool board_set(struct Board* board, int x, int y, bool value);
bool board_read(const struct Board* board, int x, int y, bool* out_value);
bool board_read_wrap(const struct Board* board, int x, int y, bool* out_value);
void board_print(const struct Board* board, bool verbose, bool neighbours);
int board_count_neighbours_alive(struct Board* b, int tx, int ty);
// Copies only board contents to dest, which should be already allocated. 
void board_copy_cells(struct Board* dest, struct Board* board);

#endif