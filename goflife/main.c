#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "board.h"
#include "game.h"

#include <SDL.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include <assert.h>

void die(const char* msg)
{
	puts(msg);
	getchar();
	exit(1);
}

static void test()
{
	struct Board* b = board_make(6, 8);
	board_clear(b);

	board_set(b, 0, 0, true);
	board_set(b, 1, 1, true);
	board_set(b, 1, 1, false);

	board_set(b, 2, 1, true);
	board_set(b, 3, 1, true);

	board_set(b, 4, 7, true);
	board_set(b, 5, 7, true);
	board_set(b, 5, 7, false);

	assert(board_count_neighbours_alive(b, 1, 2) == 1);
	assert(board_count_neighbours_alive(b, 1, 6) == 0);

	assert(!board_set(b, 100, 100, true));

	// test wrapping
	board_set(b, 0, 5, true);
	board_set(b, 5, 5, true);

	bool val = false;
	assert(board_read_wrap(b, 6, 5, &val));
	assert(val == true);
	val = false;
	assert(board_read_wrap(b, -1, 5, &val));
	assert(val == true);

	board_print(b, true, true);

	board_free(b);
}

void gameloop(struct Board* board, struct Ruleset* set) 
{
	// temporary board used to store intermediary results
	struct Board* temp_b = board_make(board->width, board->height);

	{
		if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
		{
			fprintf(stderr, "%s\n", SDL_GetError());
		}
		SDL_Window* window = SDL_CreateWindow("goflife", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1024, 600, 0);
		if (window == NULL)
		{
			fprintf(stderr, "%s\n", SDL_GetError());
			SDL_Quit();
		}
		SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);
		if (renderer == NULL)
		{
			fprintf(stderr, "%s\n", SDL_GetError());
			SDL_Quit();
		}

		SDL_Texture* image_dead = SDL_CreateTextureFromSurface(renderer, SDL_LoadBMP("dead.bmp"));
		SDL_Texture* image_alive = SDL_CreateTextureFromSurface(renderer, SDL_LoadBMP("alive.bmp"));
		SDL_Texture* image_paused = SDL_CreateTextureFromSurface(renderer, SDL_LoadBMP("paused.bmp"));
		assert(image_dead);
		assert(image_alive);
		assert(image_paused);

		uint32_t last_update = 0;
		bool running = true;
		bool paused = false;
		int mouse_x = 0;
		int mouse_y = 0;
		bool mouse_left_pressed = false;
		bool mouse_right_pressed = false;
		while (running)
		{
			SDL_RenderClear(renderer);

			for (int y = 0; y < board->height; y++) {
				for (int x = 0; x < board->width; x++) {
					bool value;
					if (!board_read(board, x, y, &value))
						continue;

					SDL_Rect dstrect = { x*16, y*16, 16, 16 };
					SDL_RenderCopy(renderer, value ? image_alive : image_dead, NULL, &dstrect);
				}
			}

			if (mouse_left_pressed || mouse_right_pressed) {
				int tx = mouse_x / 16;
				int ty = mouse_y / 16;
				bool state = mouse_left_pressed && !mouse_right_pressed;
				board_set(board, tx, ty, state);
			}

			if (paused) {
				SDL_Rect dstrect = { 1024 - 100 - 20, 0, 100, 33};
				SDL_RenderCopy(renderer, image_paused, NULL, &dstrect);
			}

			SDL_RenderPresent(renderer);

			// Reset the flags every frame, set to true in event loop if pressed.
			mouse_left_pressed = false;
			mouse_right_pressed = false;

			// Handle input and events.
			SDL_Event sdl_event;
			SDL_Scancode key;
			while (SDL_PollEvent(&sdl_event) > 0)
			{
				switch (sdl_event.type)
				{
				case SDL_KEYDOWN:
					key = sdl_event.key.keysym.scancode;
					if (key == SDL_SCANCODE_ESCAPE) {
						running = false;
					} else if (key == SDL_SCANCODE_SPACE) {
						paused = !paused;
					} 

					break;
				case SDL_MOUSEMOTION:
					mouse_x = sdl_event.motion.x;
					mouse_y = sdl_event.motion.y;
					break;
				case SDL_MOUSEBUTTONDOWN:
					if (sdl_event.button.button == 1) {
						mouse_left_pressed = true;
					} else {
						mouse_right_pressed = true;
					}
					break;
				case SDL_QUIT:
					running = false;
					break;
				}
			}

			if (SDL_GetTicks() - last_update > 500 && !paused) {
				//board_print(board, false, true);
				game_step(board, temp_b, set);
				last_update = SDL_GetTicks();
			}
		}

		SDL_DestroyRenderer(renderer);
		SDL_DestroyTexture(image_dead);
		SDL_DestroyTexture(image_alive);
		SDL_DestroyTexture(image_paused);
		SDL_Quit();
	}
}

int main(int argc, char** argv)
{
	printf("gameoflife v%s\n", GOFLIFE_GAME_VERSION);

	struct Ruleset set;
	const char* rules = "B3/S23";
	const char* size = "32x25";

	if (argc >= 2) {
		rules = argv[1];
	}

	if (!ruleset_parse(rules, &set)) {
		die("Rulesets must be of form Ba/Sb, where a and be are both lists of single digit integers.");
	}

	ruleset_print(&set);

	if (argc >= 3) {
		size = argv[2];
	}

	int width, height;

	if (sscanf(size, "%dx%d", &width, &height) != 2) {
		die("Board size must be of form WxY.");
	}

	test();

	struct Board* board = board_make(width, height);
	// oscillator
	board_set(board, 2, 3, true);
	board_set(board, 3, 3, true);
	board_set(board, 4, 3, true);

	gameloop(board, &set);

	board_free(board);

	return 0;
}