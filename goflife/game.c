#include "game.h"

#include <string.h>
#include <assert.h>

// Writes the parsed ruleset to out_set if its well formed.
bool ruleset_parse(const char* str, struct Ruleset* out_set)
{
	struct Ruleset set = {0};

	// First validate that the string is of the correct form.
	int b_count, s_count;
	if (sscanf(str, "B%d/S%d", &b_count, &s_count) != 2) {
		return false;
	}

	printf("%d %d\n", b_count, s_count);

	// Read the rules in right-to-left order.
	int b = b_count;
	int i;
	for (i = 0; (i < RULESET_COUNT_MAX) && b; i++) {
		int digit = b % 10;
		set.born_rules[i] = digit;
		b /= 10;
	}

	set.born_rule_count = i;

	int s = s_count;
	for (i = 0; (i < RULESET_COUNT_MAX) && s; i++) {
		int digit = s % 10;
		set.stays_rules[i] = digit;
		s /= 10;
	}

	set.stays_rule_count = i;

	*out_set = set;
	return true;
}

void ruleset_print(struct Ruleset* set)
{
	printf("Ruleset %d %d\n", set->born_rule_count, set->stays_rule_count);
	for (int a = 0; a < set->born_rule_count; a++) {
		printf("\tborn: %d\n", set->born_rules[a]);
	}

	for (int d = 0; d < set->stays_rule_count; d++) {
		printf("\tstays: %d\n", set->stays_rules[d]);
	}
}

static bool list_contains(int needle, int* haystack, int count)
{
	for (int i = 0; i < count; i++) {
		if (haystack[i] == needle)
			return true;
	}

	return false;
}

void game_step(struct Board* board, struct Board* temp_board, struct Ruleset* set)
{
	// We make a copy where to calculate neighbour count from.
	board_copy_cells(temp_board, board);

	for (int y=0; y<board->height; y++) {
		for (int x=0; x<board->width; x++) {
			int n = board_count_neighbours_alive(temp_board, x, y);
			bool state;

			if (!board_read(temp_board, x, y, &state)) {
				assert(false);
			} 

			bool newstate = state;

			//printf("(%d, %d) neighbours: %d\n", x, y, n);

			if (state) {
				newstate = list_contains(n, set->stays_rules, set->stays_rule_count);
			} else {
				newstate = list_contains(n, set->born_rules, set->born_rule_count);
			}

			board_set(board, x, y, newstate);
		}
	}
}

