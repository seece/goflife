#include "board.h"
#include <stdlib.h>
#include <assert.h>
#include <string.h>

struct Board* board_make(int width, int height)
{
	if (width <= 0 || height <= 0) {
		return NULL;
	}

	struct Board* board = malloc(sizeof(struct Board));
	if (!board) {
		return NULL;
	}

	board->width = width;
	board->height = height;
	//board->size = ceil((width*height) / 8.0); // this fails horribly for some reason on MSVC2013
	board->size = (width*height) / 8 + 1;
	board->cells = malloc(board->size);

	if (!board->cells) {
		free(board);
		return NULL;
	}

	board_clear(board);

	return board;
}

void board_free(struct Board* board)
{
	if (!board) {
		return;
	}

	free(board->cells);
	free(board);
}

void board_clear(struct Board* board)
{
	memset(board->cells, 0, board->size);
}

static bool board_get_offset(const struct Board* board, int x, int y, int* out_byte_ofs, int* out_bit_ofs)
{
	int index = board->width*y + x;
	int byte_ofs = index / 8;
	int bit_ofs = index % 8;
	
	assert(bit_ofs < 8);

	if (byte_ofs >= board->size) {
		return false; // out of bounds
	}

	*out_byte_ofs = byte_ofs;
	*out_bit_ofs = bit_ofs;

	return true;
}

bool board_set(struct Board* board, int x, int y, bool value)
{
	int index = 0;
	int byte_ofs = 0;
	int bit_ofs = 0;

	if (!board_get_offset(board, x, y, &byte_ofs, &bit_ofs)) {
		return false;
	}

	uint8_t old = board->cells[byte_ofs];
	old = old & ~(1 << bit_ofs); //unset bit

	// set the bit if value is set, otherwise keep as zero
	if (value) {
		board->cells[byte_ofs] = old | (1 << bit_ofs);
	} else {
		board->cells[byte_ofs] = old;
	}


	return true;
}

bool board_read(const struct Board* board, int x, int y, bool* out_value)
{
	int index = 0;
	int byte_ofs = 0;
	int bit_ofs = 0;

	if (!board_get_offset(board, x, y, &byte_ofs, &bit_ofs)) {
		return false;
	}

	*out_value = (board->cells[byte_ofs] & (1 << bit_ofs)) != 0;

	return true;
}

bool board_read_wrap(const struct Board* board, int x, int y, bool* out_value)
{
	if (x < 0) {
		x = board->width + x;
	}

	if (y < 0) {
		y = board->height + y;
	}

	return board_read(board, x % board->width, y % board->height, out_value);
}

void board_print(const struct Board* board, bool verbose, bool neighbours)
{
	if (verbose)
		printf("Board size %dx%d, %d bytes. data = %p\n", board->width, board->height, board->size, board->cells);

	for (int y=0;y<board->height;y++) {
		for (int x=0;x<board->width;x++) {
			bool value;
			if (board_read(board, x, y, &value)) {
				printf("%c", value ? 'X' : '.');
			} else {
				printf("Error couldn't read value.");
			}
		}

		if (neighbours) {
			printf(" ");

			for (int x=0;x<board->width;x++) {
				int n = board_count_neighbours_alive(board, x, y);
				if (n > 0) {
					printf("%d", n);
				} else {
					printf(".", n);
				}
			}
		}
		printf("\n");
	}
}


int board_count_neighbours_alive(struct Board* b, int tx, int ty)
{
	assert(b);
	int count = 0;
	for (int y = -1; y < 2; y++) {
		for (int x = -1; x < 2; x++) {
			if (y == 0 && x == 0)
				continue;

			bool val = false;

			if (board_read_wrap(b, tx + x, ty + y, &val)) {
				if (val)
					count++;
			} else {
				assert(false);
			}
		}
	}

	return count;
}

void board_copy_cells(struct Board* dest, struct Board* board)
{
	memcpy(dest->cells, board->cells, board->size);
}
