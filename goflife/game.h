#ifndef GAME_H
#define GAME_H

#include "board.h"

#define GOFLIFE_GAME_VERSION "1"
#define RULESET_COUNT_MAX (10)

struct Ruleset {
	int born_rule_count;
	int stays_rule_count;
	int born_rules[RULESET_COUNT_MAX];
	int stays_rules[RULESET_COUNT_MAX];
};

// Writes the parsed ruleset to out_set if its well formed.
bool ruleset_parse(const char* str, struct Ruleset* out_set);
void ruleset_print(struct Ruleset* set);
bool list_contains(int needle, int* haystack, int count);
void game_step(struct Board* board, struct Board* temp_board, struct Ruleset* set);

#endif